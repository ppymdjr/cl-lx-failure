#!/bin/bash

# Stage 2 installed - runs in the vm...
echo "Executing stage 2 installer"

mkdir -p /usr/local/src/
cd /usr/local/src/

echo "Downloading CCL..."
wget --quiet --no-check-certificate https://github.com/Clozure/ccl/releases/download/v1.11.5/ccl-1.11.5-linuxx86.tar.gz

echo "extracting..."
tar xzf ccl-1.11.5-linuxx86.tar.gz
rm ccl-1.11.5-linuxx86.tar.gz

ln -s /usr/local/src/ccl/scripts/ccl64 /usr/local/bin/
cd /root


curl -kO https://beta.quicklisp.org/quicklisp.lisp

/usr/local/bin/ccl64 -l quicklisp.lisp -e '(quicklisp-quickstart:install)' -e '(ccl:quit)'
/usr/local/bin/ccl64 -l quicklisp/setup.lisp -e '(ql:quickload :cl-json)' -l fail.lisp -e '(ccl:quit)'
