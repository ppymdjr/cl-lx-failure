#!/bin/bash

# to update this run the following shell command:-
# 
# IMAGE=`imgadm avail -H -ouuid name=base-64-lts | tail -n 1`

IMAGE='7b5981c4-1889-11e7-b4c5-3f3bdfc9b88b'
ALIAS='cl-lx-failure'
# It would be nice to find an available IP address
IP='10.10.0.93'

imgadm import $IMAGE

# find the existing VM...
EXISTING=`vmadm lookup nics.0.ip=$IP alias=$ALIAS`
EXISTING_IMAGE=`vmadm list -H -o image_uuid uuid=$EXISTING`

# then if the existing VM has the right image we could just update its config
# just run the stage 2 and don't do the destroy/recreate?
# although we won't need to do all the stage2. Maybe I should make a stage 3 as well

if [[ -n $EXISTING ]]; then
    echo "Destroying existing vm $EXISTING..."
    vmadm destroy $EXISTING
fi


echo "Creating VM"
vmadm create 2>vm-uuid <<EOF
{
  "autoboot": true,
  "brand": "lx",
  "image_uuid": "$IMAGE",
  "max_physical_memory": 256,
  "quota": 0,
  "hostname": "$ALIAS",
  "alias": "$ALIAS",
  "dns_domain": "local",
  "kernel_version": "3.13.0",
  "resolvers": [
    "8.8.8.8",
    "8.8.4.4"
  ],
  "nics": [
    {
      "nic_tag":"stub0",
      "gateway": "10.10.0.1",
      "netmask": "255.255.255.0",
      "ip": "$IP",
      "primary": true
    }
  ]
}

EOF

if [ "$?" -ne "0" ]; then
    cat vm-uuid
    exit 1
fi

UUID=`cat vm-uuid`
UUID=${UUID#Successfully created VM }

echo "Created VM '$UUID'"

cp stage2.sh /zones/$UUID/root/
cp fail.lisp /zones/$UUID/root/root/

zlogin $UUID sh /stage2.sh

GIT_DIR=.git

git config user.email robot@example.com
git config user.name $UUID

# If we need to commit any build artifacts we can do so here...
# git add something && git commit -m "Rebuilt zone" && git push origin master


# All done
exit 0
