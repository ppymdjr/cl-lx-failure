#!/bin/bash

TARGET=$1

# login to the VM tied to this repository...
ME=`dirname $0`
# ME=${ME%login}

# it would be good if we had a general way to run commands on the deployed zone
# I should look into that.
# Maybe I should put that in the deploy scripts, although it's peculiar to zones
# It might be worth making a base repository (and merging it in perhaps) for repos which make zones
# This kind of logic should be extended to handle arbitrary commands on remotes
# BUT If I want to run arbitrary things I need to do pass in a shell script to get the vm-uuid from
# the remote and not depend on this same script to do so

# SO, I need a general executor as well as specific verbs like login, destroy and so on
# would also be good to have stop, start etc etc etc

# It's probably worth putting the zone VM template directory somewhere
# so I can create templates as starting points. There will be a common
# pattern here

# creating a new VM repo will just be a case of cloning the template repository and then changing it
# probably disconnecting it from the origin by removing that (maybe - although it would be useful to rebase)
if [[ -n $TARGET ]]; then
    A=`git config --get remote.$TARGET.url`
    if [[ -n $A ]]; then
        A=${A%.git}
        A=${A#$TARGET:/git-repositories/}
        echo "Connecting to /git-repositories/build/$A/login..."
        ssh -t $TARGET /git-repositories/build/$A/login
        exit 0
    else
        echo "Not deployed on $TARGET"
        exit 1
    fi
fi

UUID=`cat $ME/vm-uuid`
UUID=${UUID#Successfully created VM }

zlogin $UUID
