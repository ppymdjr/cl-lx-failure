#+TITLE: Clozure Common Lisp lx Zone Failure

* Introduction
This is a quick demonstration (or test) of the failure of Clozure
Common Lisp to run properly in an lx-branded (ie Linux syscall
emulation) zone on SmartOS (and other illumos distributions).

Unfortunately despite some work towards fixing this (citation needed)
it still doesn't work.
