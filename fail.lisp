(in-package :json)

;; (encode-json-to-string (loop for i from 1 to 70 collect '(:d . 1)))
;; (format t "done.~%")

(loop for i from 1 to 70 ; 100000
        do
        (print i)
        (force-output *standard-output*)
        (encode-json '((:d . 1))
                     *standard-output*))
